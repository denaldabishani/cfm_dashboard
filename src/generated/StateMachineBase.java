/**
 * This class contains generated code from the Codename One Designer, DO NOT MODIFY!
 * This class is designed for subclassing that way the code generator can overwrite it
 * anytime without erasing your changes which should exist in a subclass!
 * For details about this file and how it works please read this blog post:
 * http://codenameone.blogspot.com/2010/10/ui-builder-class-how-to-actually-use.html
*/
package generated;

import com.codename1.ui.*;
import com.codename1.ui.util.*;
import com.codename1.ui.plaf.*;
import java.util.Hashtable;
import com.codename1.ui.events.*;

public abstract class StateMachineBase extends UIBuilder {
    private Container aboutToShowThisContainer;
    /**
     * this method should be used to initialize variables instead of
     * the constructor/class scope to avoid race conditions
     */
    /**
    * @deprecated use the version that accepts a resource as an argument instead
    
**/
    protected void initVars() {}

    protected void initVars(Resources res) {}

    public StateMachineBase(Resources res, String resPath, boolean loadTheme) {
        startApp(res, resPath, loadTheme);
    }

    public Container startApp(Resources res, String resPath, boolean loadTheme) {
        initVars();
        UIBuilder.registerCustomComponent("Container", com.codename1.ui.Container.class);
        UIBuilder.registerCustomComponent("Form", com.codename1.ui.Form.class);
        UIBuilder.registerCustomComponent("Button", com.codename1.ui.Button.class);
        UIBuilder.registerCustomComponent("Label", com.codename1.ui.Label.class);
        UIBuilder.registerCustomComponent("TextField", com.codename1.ui.TextField.class);
        UIBuilder.registerCustomComponent("Tabs", com.codename1.ui.Tabs.class);
        if(loadTheme) {
            if(res == null) {
                try {
                    if(resPath.endsWith(".res")) {
                        res = Resources.open(resPath);
                        System.out.println("Warning: you should construct the state machine without the .res extension to allow theme overlays");
                    } else {
                        res = Resources.openLayered(resPath);
                    }
                } catch(java.io.IOException err) { err.printStackTrace(); }
            }
            initTheme(res);
        }
        if(res != null) {
            setResourceFilePath(resPath);
            setResourceFile(res);
            initVars(res);
            return showForm(getFirstFormName(), null);
        } else {
            Form f = (Form)createContainer(resPath, getFirstFormName());
            initVars(fetchResourceFile());
            beforeShow(f);
            f.show();
            postShow(f);
            return f;
        }
    }

    protected String getFirstFormName() {
        return "MainMenu";
    }

    public Container createWidget(Resources res, String resPath, boolean loadTheme) {
        initVars();
        UIBuilder.registerCustomComponent("Container", com.codename1.ui.Container.class);
        UIBuilder.registerCustomComponent("Form", com.codename1.ui.Form.class);
        UIBuilder.registerCustomComponent("Button", com.codename1.ui.Button.class);
        UIBuilder.registerCustomComponent("Label", com.codename1.ui.Label.class);
        UIBuilder.registerCustomComponent("TextField", com.codename1.ui.TextField.class);
        UIBuilder.registerCustomComponent("Tabs", com.codename1.ui.Tabs.class);
        if(loadTheme) {
            if(res == null) {
                try {
                    res = Resources.openLayered(resPath);
                } catch(java.io.IOException err) { err.printStackTrace(); }
            }
            initTheme(res);
        }
        return createContainer(resPath, "MainMenu");
    }

    protected void initTheme(Resources res) {
            String[] themes = res.getThemeResourceNames();
            if(themes != null && themes.length > 0) {
                UIManager.getInstance().setThemeProps(res.getTheme(themes[0]));
            }
    }

    public StateMachineBase() {
    }

    public StateMachineBase(String resPath) {
        this(null, resPath, true);
    }

    public StateMachineBase(Resources res) {
        this(res, null, true);
    }

    public StateMachineBase(String resPath, boolean loadTheme) {
        this(null, resPath, loadTheme);
    }

    public StateMachineBase(Resources res, boolean loadTheme) {
        this(res, null, loadTheme);
    }

    public com.codename1.ui.Label findLabel11(Component root) {
        return (com.codename1.ui.Label)findByName("Label11", root);
    }

    public com.codename1.ui.Label findLabel11() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label11", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label11", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel12(Component root) {
        return (com.codename1.ui.Label)findByName("Label12", root);
    }

    public com.codename1.ui.Label findLabel12() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label12", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label12", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer9(Component root) {
        return (com.codename1.ui.Container)findByName("Container9", root);
    }

    public com.codename1.ui.Container findContainer9() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container9", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container9", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel13(Component root) {
        return (com.codename1.ui.Label)findByName("Label13", root);
    }

    public com.codename1.ui.Label findLabel13() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label13", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label13", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel14(Component root) {
        return (com.codename1.ui.Label)findByName("Label14", root);
    }

    public com.codename1.ui.Label findLabel14() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label14", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label14", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findButton1(Component root) {
        return (com.codename1.ui.Button)findByName("Button1", root);
    }

    public com.codename1.ui.Button findButton1() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("Button1", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("Button1", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel10(Component root) {
        return (com.codename1.ui.Label)findByName("Label10", root);
    }

    public com.codename1.ui.Label findLabel10() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label10", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label10", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel(Component root) {
        return (com.codename1.ui.Label)findByName("Label", root);
    }

    public com.codename1.ui.Label findLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel19(Component root) {
        return (com.codename1.ui.Label)findByName("Label19", root);
    }

    public com.codename1.ui.Label findLabel19() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label19", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label19", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer11(Component root) {
        return (com.codename1.ui.Container)findByName("Container11", root);
    }

    public com.codename1.ui.Container findContainer11() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container11", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container11", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer3(Component root) {
        return (com.codename1.ui.Container)findByName("Container3", root);
    }

    public com.codename1.ui.Container findContainer3() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container3", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container3", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer10(Component root) {
        return (com.codename1.ui.Container)findByName("Container10", root);
    }

    public com.codename1.ui.Container findContainer10() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container10", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container10", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer4(Component root) {
        return (com.codename1.ui.Container)findByName("Container4", root);
    }

    public com.codename1.ui.Container findContainer4() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container4", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container4", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer1(Component root) {
        return (com.codename1.ui.Container)findByName("Container1", root);
    }

    public com.codename1.ui.Container findContainer1() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container1", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container1", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer13(Component root) {
        return (com.codename1.ui.Container)findByName("Container13", root);
    }

    public com.codename1.ui.Container findContainer13() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container13", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container13", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer2(Component root) {
        return (com.codename1.ui.Container)findByName("Container2", root);
    }

    public com.codename1.ui.Container findContainer2() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container2", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container2", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer12(Component root) {
        return (com.codename1.ui.Container)findByName("Container12", root);
    }

    public com.codename1.ui.Container findContainer12() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container12", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container12", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer7(Component root) {
        return (com.codename1.ui.Container)findByName("Container7", root);
    }

    public com.codename1.ui.Container findContainer7() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container7", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container7", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel15(Component root) {
        return (com.codename1.ui.Label)findByName("Label15", root);
    }

    public com.codename1.ui.Label findLabel15() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label15", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label15", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer15(Component root) {
        return (com.codename1.ui.Container)findByName("Container15", root);
    }

    public com.codename1.ui.Container findContainer15() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container15", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container15", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer8(Component root) {
        return (com.codename1.ui.Container)findByName("Container8", root);
    }

    public com.codename1.ui.Container findContainer8() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container8", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container8", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel16(Component root) {
        return (com.codename1.ui.Label)findByName("Label16", root);
    }

    public com.codename1.ui.Label findLabel16() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label16", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label16", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer14(Component root) {
        return (com.codename1.ui.Container)findByName("Container14", root);
    }

    public com.codename1.ui.Container findContainer14() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container14", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container14", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel17(Component root) {
        return (com.codename1.ui.Label)findByName("Label17", root);
    }

    public com.codename1.ui.Label findLabel17() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label17", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label17", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer5(Component root) {
        return (com.codename1.ui.Container)findByName("Container5", root);
    }

    public com.codename1.ui.Container findContainer5() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container5", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container5", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findPaginationContainer(Component root) {
        return (com.codename1.ui.Container)findByName("PaginationContainer", root);
    }

    public com.codename1.ui.Container findPaginationContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("PaginationContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("PaginationContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findLegendContainer(Component root) {
        return (com.codename1.ui.Container)findByName("LegendContainer", root);
    }

    public com.codename1.ui.Container findLegendContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("LegendContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("LegendContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer17(Component root) {
        return (com.codename1.ui.Container)findByName("Container17", root);
    }

    public com.codename1.ui.Container findContainer17() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container17", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container17", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel18(Component root) {
        return (com.codename1.ui.Label)findByName("Label18", root);
    }

    public com.codename1.ui.Label findLabel18() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label18", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label18", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer6(Component root) {
        return (com.codename1.ui.Container)findByName("Container6", root);
    }

    public com.codename1.ui.Container findContainer6() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container6", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container6", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer16(Component root) {
        return (com.codename1.ui.Container)findByName("Container16", root);
    }

    public com.codename1.ui.Container findContainer16() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container16", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container16", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer(Component root) {
        return (com.codename1.ui.Container)findByName("Container", root);
    }

    public com.codename1.ui.Container findContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findButton(Component root) {
        return (com.codename1.ui.Button)findByName("Button", root);
    }

    public com.codename1.ui.Button findButton() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("Button", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("Button", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.TextField findTextField(Component root) {
        return (com.codename1.ui.TextField)findByName("TextField", root);
    }

    public com.codename1.ui.TextField findTextField() {
        com.codename1.ui.TextField cmp = (com.codename1.ui.TextField)findByName("TextField", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.TextField)findByName("TextField", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Tabs findTabs(Component root) {
        return (com.codename1.ui.Tabs)findByName("Tabs", root);
    }

    public com.codename1.ui.Tabs findTabs() {
        com.codename1.ui.Tabs cmp = (com.codename1.ui.Tabs)findByName("Tabs", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Tabs)findByName("Tabs", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findButton2(Component root) {
        return (com.codename1.ui.Button)findByName("Button2", root);
    }

    public com.codename1.ui.Button findButton2() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("Button2", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("Button2", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findDetailsContainer(Component root) {
        return (com.codename1.ui.Container)findByName("DetailsContainer", root);
    }

    public com.codename1.ui.Container findDetailsContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("DetailsContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("DetailsContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel30(Component root) {
        return (com.codename1.ui.Label)findByName("Label30", root);
    }

    public com.codename1.ui.Label findLabel30() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label30", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label30", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.TextField findTextField1(Component root) {
        return (com.codename1.ui.TextField)findByName("TextField1", root);
    }

    public com.codename1.ui.TextField findTextField1() {
        com.codename1.ui.TextField cmp = (com.codename1.ui.TextField)findByName("TextField1", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.TextField)findByName("TextField1", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainerList(Component root) {
        return (com.codename1.ui.Container)findByName("ContainerList", root);
    }

    public com.codename1.ui.Container findContainerList() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("ContainerList", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("ContainerList", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findChartContainer(Component root) {
        return (com.codename1.ui.Container)findByName("ChartContainer", root);
    }

    public com.codename1.ui.Container findChartContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("ChartContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("ChartContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel9(Component root) {
        return (com.codename1.ui.Label)findByName("Label9", root);
    }

    public com.codename1.ui.Label findLabel9() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label9", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label9", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel8(Component root) {
        return (com.codename1.ui.Label)findByName("Label8", root);
    }

    public com.codename1.ui.Label findLabel8() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label8", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label8", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel5(Component root) {
        return (com.codename1.ui.Label)findByName("Label5", root);
    }

    public com.codename1.ui.Label findLabel5() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label5", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label5", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel4(Component root) {
        return (com.codename1.ui.Label)findByName("Label4", root);
    }

    public com.codename1.ui.Label findLabel4() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label4", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label4", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel7(Component root) {
        return (com.codename1.ui.Label)findByName("Label7", root);
    }

    public com.codename1.ui.Label findLabel7() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label7", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label7", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel6(Component root) {
        return (com.codename1.ui.Label)findByName("Label6", root);
    }

    public com.codename1.ui.Label findLabel6() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label6", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label6", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findListContainer(Component root) {
        return (com.codename1.ui.Container)findByName("ListContainer", root);
    }

    public com.codename1.ui.Container findListContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("ListContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("ListContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel22(Component root) {
        return (com.codename1.ui.Label)findByName("Label22", root);
    }

    public com.codename1.ui.Label findLabel22() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label22", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label22", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel23(Component root) {
        return (com.codename1.ui.Label)findByName("Label23", root);
    }

    public com.codename1.ui.Label findLabel23() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label23", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label23", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel24(Component root) {
        return (com.codename1.ui.Label)findByName("Label24", root);
    }

    public com.codename1.ui.Label findLabel24() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label24", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label24", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel25(Component root) {
        return (com.codename1.ui.Label)findByName("Label25", root);
    }

    public com.codename1.ui.Label findLabel25() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label25", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label25", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel20(Component root) {
        return (com.codename1.ui.Label)findByName("Label20", root);
    }

    public com.codename1.ui.Label findLabel20() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label20", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label20", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel21(Component root) {
        return (com.codename1.ui.Label)findByName("Label21", root);
    }

    public com.codename1.ui.Label findLabel21() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label21", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label21", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel1(Component root) {
        return (com.codename1.ui.Label)findByName("Label1", root);
    }

    public com.codename1.ui.Label findLabel1() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label1", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label1", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel3(Component root) {
        return (com.codename1.ui.Label)findByName("Label3", root);
    }

    public com.codename1.ui.Label findLabel3() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label3", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label3", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel2(Component root) {
        return (com.codename1.ui.Label)findByName("Label2", root);
    }

    public com.codename1.ui.Label findLabel2() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label2", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label2", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel26(Component root) {
        return (com.codename1.ui.Label)findByName("Label26", root);
    }

    public com.codename1.ui.Label findLabel26() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label26", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label26", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel27(Component root) {
        return (com.codename1.ui.Label)findByName("Label27", root);
    }

    public com.codename1.ui.Label findLabel27() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label27", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label27", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findLayer(Component root) {
        return (com.codename1.ui.Container)findByName("Layer", root);
    }

    public com.codename1.ui.Container findLayer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Layer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Layer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel28(Component root) {
        return (com.codename1.ui.Label)findByName("Label28", root);
    }

    public com.codename1.ui.Label findLabel28() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label28", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label28", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findComponentsContainer(Component root) {
        return (com.codename1.ui.Container)findByName("ComponentsContainer", root);
    }

    public com.codename1.ui.Container findComponentsContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("ComponentsContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("ComponentsContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel29(Component root) {
        return (com.codename1.ui.Label)findByName("Label29", root);
    }

    public com.codename1.ui.Label findLabel29() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label29", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label29", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findNonQualitativeContainer(Component root) {
        return (com.codename1.ui.Container)findByName("NonQualitativeContainer", root);
    }

    public com.codename1.ui.Container findNonQualitativeContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("NonQualitativeContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("NonQualitativeContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findQualitativeContainer(Component root) {
        return (com.codename1.ui.Container)findByName("QualitativeContainer", root);
    }

    public com.codename1.ui.Container findQualitativeContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("QualitativeContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("QualitativeContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    protected void exitForm(Form f) {
        if("Login".equals(f.getName())) {
            exitLogin(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Monitoring".equals(f.getName())) {
            exitMonitoring(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Dashboard".equals(f.getName())) {
            exitDashboard(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("MainMenu".equals(f.getName())) {
            exitMainMenu(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("QualitativeReport".equals(f.getName())) {
            exitQualitativeReport(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Test".equals(f.getName())) {
            exitTest(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("MainDashboard".equals(f.getName())) {
            exitMainDashboard(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("SurveyDetails".equals(f.getName())) {
            exitSurveyDetails(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("NonQualitativeReport".equals(f.getName())) {
            exitNonQualitativeReport(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("SignInForm".equals(f.getName())) {
            exitSignInForm(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Surveys".equals(f.getName())) {
            exitSurveys(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Main".equals(f.getName())) {
            exitMain(f);
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void exitLogin(Form f) {
    }


    protected void exitMonitoring(Form f) {
    }


    protected void exitDashboard(Form f) {
    }


    protected void exitMainMenu(Form f) {
    }


    protected void exitQualitativeReport(Form f) {
    }


    protected void exitTest(Form f) {
    }


    protected void exitMainDashboard(Form f) {
    }


    protected void exitSurveyDetails(Form f) {
    }


    protected void exitNonQualitativeReport(Form f) {
    }


    protected void exitSignInForm(Form f) {
    }


    protected void exitSurveys(Form f) {
    }


    protected void exitMain(Form f) {
    }

    protected void beforeShow(Form f) {
    aboutToShowThisContainer = f;
        if("Login".equals(f.getName())) {
            beforeLogin(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Monitoring".equals(f.getName())) {
            beforeMonitoring(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Dashboard".equals(f.getName())) {
            beforeDashboard(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("MainMenu".equals(f.getName())) {
            beforeMainMenu(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("QualitativeReport".equals(f.getName())) {
            beforeQualitativeReport(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Test".equals(f.getName())) {
            beforeTest(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("MainDashboard".equals(f.getName())) {
            beforeMainDashboard(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("SurveyDetails".equals(f.getName())) {
            beforeSurveyDetails(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("NonQualitativeReport".equals(f.getName())) {
            beforeNonQualitativeReport(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("SignInForm".equals(f.getName())) {
            beforeSignInForm(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Surveys".equals(f.getName())) {
            beforeSurveys(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Main".equals(f.getName())) {
            beforeMain(f);
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void beforeLogin(Form f) {
    }


    protected void beforeMonitoring(Form f) {
    }


    protected void beforeDashboard(Form f) {
    }


    protected void beforeMainMenu(Form f) {
    }


    protected void beforeQualitativeReport(Form f) {
    }


    protected void beforeTest(Form f) {
    }


    protected void beforeMainDashboard(Form f) {
    }


    protected void beforeSurveyDetails(Form f) {
    }


    protected void beforeNonQualitativeReport(Form f) {
    }


    protected void beforeSignInForm(Form f) {
    }


    protected void beforeSurveys(Form f) {
    }


    protected void beforeMain(Form f) {
    }

    protected void beforeShowContainer(Container c) {
        aboutToShowThisContainer = c;
        if("Login".equals(c.getName())) {
            beforeContainerLogin(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Monitoring".equals(c.getName())) {
            beforeContainerMonitoring(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Dashboard".equals(c.getName())) {
            beforeContainerDashboard(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("MainMenu".equals(c.getName())) {
            beforeContainerMainMenu(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("QualitativeReport".equals(c.getName())) {
            beforeContainerQualitativeReport(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Test".equals(c.getName())) {
            beforeContainerTest(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("MainDashboard".equals(c.getName())) {
            beforeContainerMainDashboard(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("SurveyDetails".equals(c.getName())) {
            beforeContainerSurveyDetails(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("NonQualitativeReport".equals(c.getName())) {
            beforeContainerNonQualitativeReport(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("SignInForm".equals(c.getName())) {
            beforeContainerSignInForm(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Surveys".equals(c.getName())) {
            beforeContainerSurveys(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Main".equals(c.getName())) {
            beforeContainerMain(c);
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void beforeContainerLogin(Container c) {
    }


    protected void beforeContainerMonitoring(Container c) {
    }


    protected void beforeContainerDashboard(Container c) {
    }


    protected void beforeContainerMainMenu(Container c) {
    }


    protected void beforeContainerQualitativeReport(Container c) {
    }


    protected void beforeContainerTest(Container c) {
    }


    protected void beforeContainerMainDashboard(Container c) {
    }


    protected void beforeContainerSurveyDetails(Container c) {
    }


    protected void beforeContainerNonQualitativeReport(Container c) {
    }


    protected void beforeContainerSignInForm(Container c) {
    }


    protected void beforeContainerSurveys(Container c) {
    }


    protected void beforeContainerMain(Container c) {
    }

    protected void postShow(Form f) {
        if("Login".equals(f.getName())) {
            postLogin(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Monitoring".equals(f.getName())) {
            postMonitoring(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Dashboard".equals(f.getName())) {
            postDashboard(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("MainMenu".equals(f.getName())) {
            postMainMenu(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("QualitativeReport".equals(f.getName())) {
            postQualitativeReport(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Test".equals(f.getName())) {
            postTest(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("MainDashboard".equals(f.getName())) {
            postMainDashboard(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("SurveyDetails".equals(f.getName())) {
            postSurveyDetails(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("NonQualitativeReport".equals(f.getName())) {
            postNonQualitativeReport(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("SignInForm".equals(f.getName())) {
            postSignInForm(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Surveys".equals(f.getName())) {
            postSurveys(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Main".equals(f.getName())) {
            postMain(f);
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void postLogin(Form f) {
    }


    protected void postMonitoring(Form f) {
    }


    protected void postDashboard(Form f) {
    }


    protected void postMainMenu(Form f) {
    }


    protected void postQualitativeReport(Form f) {
    }


    protected void postTest(Form f) {
    }


    protected void postMainDashboard(Form f) {
    }


    protected void postSurveyDetails(Form f) {
    }


    protected void postNonQualitativeReport(Form f) {
    }


    protected void postSignInForm(Form f) {
    }


    protected void postSurveys(Form f) {
    }


    protected void postMain(Form f) {
    }

    protected void postShowContainer(Container c) {
        if("Login".equals(c.getName())) {
            postContainerLogin(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Monitoring".equals(c.getName())) {
            postContainerMonitoring(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Dashboard".equals(c.getName())) {
            postContainerDashboard(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("MainMenu".equals(c.getName())) {
            postContainerMainMenu(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("QualitativeReport".equals(c.getName())) {
            postContainerQualitativeReport(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Test".equals(c.getName())) {
            postContainerTest(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("MainDashboard".equals(c.getName())) {
            postContainerMainDashboard(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("SurveyDetails".equals(c.getName())) {
            postContainerSurveyDetails(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("NonQualitativeReport".equals(c.getName())) {
            postContainerNonQualitativeReport(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("SignInForm".equals(c.getName())) {
            postContainerSignInForm(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Surveys".equals(c.getName())) {
            postContainerSurveys(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Main".equals(c.getName())) {
            postContainerMain(c);
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void postContainerLogin(Container c) {
    }


    protected void postContainerMonitoring(Container c) {
    }


    protected void postContainerDashboard(Container c) {
    }


    protected void postContainerMainMenu(Container c) {
    }


    protected void postContainerQualitativeReport(Container c) {
    }


    protected void postContainerTest(Container c) {
    }


    protected void postContainerMainDashboard(Container c) {
    }


    protected void postContainerSurveyDetails(Container c) {
    }


    protected void postContainerNonQualitativeReport(Container c) {
    }


    protected void postContainerSignInForm(Container c) {
    }


    protected void postContainerSurveys(Container c) {
    }


    protected void postContainerMain(Container c) {
    }

    protected void onCreateRoot(String rootName) {
        if("Login".equals(rootName)) {
            onCreateLogin();
            aboutToShowThisContainer = null;
            return;
        }

        if("Monitoring".equals(rootName)) {
            onCreateMonitoring();
            aboutToShowThisContainer = null;
            return;
        }

        if("Dashboard".equals(rootName)) {
            onCreateDashboard();
            aboutToShowThisContainer = null;
            return;
        }

        if("MainMenu".equals(rootName)) {
            onCreateMainMenu();
            aboutToShowThisContainer = null;
            return;
        }

        if("QualitativeReport".equals(rootName)) {
            onCreateQualitativeReport();
            aboutToShowThisContainer = null;
            return;
        }

        if("Test".equals(rootName)) {
            onCreateTest();
            aboutToShowThisContainer = null;
            return;
        }

        if("MainDashboard".equals(rootName)) {
            onCreateMainDashboard();
            aboutToShowThisContainer = null;
            return;
        }

        if("SurveyDetails".equals(rootName)) {
            onCreateSurveyDetails();
            aboutToShowThisContainer = null;
            return;
        }

        if("NonQualitativeReport".equals(rootName)) {
            onCreateNonQualitativeReport();
            aboutToShowThisContainer = null;
            return;
        }

        if("SignInForm".equals(rootName)) {
            onCreateSignInForm();
            aboutToShowThisContainer = null;
            return;
        }

        if("Surveys".equals(rootName)) {
            onCreateSurveys();
            aboutToShowThisContainer = null;
            return;
        }

        if("Main".equals(rootName)) {
            onCreateMain();
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void onCreateLogin() {
    }


    protected void onCreateMonitoring() {
    }


    protected void onCreateDashboard() {
    }


    protected void onCreateMainMenu() {
    }


    protected void onCreateQualitativeReport() {
    }


    protected void onCreateTest() {
    }


    protected void onCreateMainDashboard() {
    }


    protected void onCreateSurveyDetails() {
    }


    protected void onCreateNonQualitativeReport() {
    }


    protected void onCreateSignInForm() {
    }


    protected void onCreateSurveys() {
    }


    protected void onCreateMain() {
    }

    protected Hashtable getFormState(Form f) {
        Hashtable h = super.getFormState(f);
        if("Login".equals(f.getName())) {
            getStateLogin(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("Monitoring".equals(f.getName())) {
            getStateMonitoring(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("Dashboard".equals(f.getName())) {
            getStateDashboard(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("MainMenu".equals(f.getName())) {
            getStateMainMenu(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("QualitativeReport".equals(f.getName())) {
            getStateQualitativeReport(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("Test".equals(f.getName())) {
            getStateTest(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("MainDashboard".equals(f.getName())) {
            getStateMainDashboard(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("SurveyDetails".equals(f.getName())) {
            getStateSurveyDetails(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("NonQualitativeReport".equals(f.getName())) {
            getStateNonQualitativeReport(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("SignInForm".equals(f.getName())) {
            getStateSignInForm(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("Surveys".equals(f.getName())) {
            getStateSurveys(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("Main".equals(f.getName())) {
            getStateMain(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

            return h;
    }


    protected void getStateLogin(Form f, Hashtable h) {
    }


    protected void getStateMonitoring(Form f, Hashtable h) {
    }


    protected void getStateDashboard(Form f, Hashtable h) {
    }


    protected void getStateMainMenu(Form f, Hashtable h) {
    }


    protected void getStateQualitativeReport(Form f, Hashtable h) {
    }


    protected void getStateTest(Form f, Hashtable h) {
    }


    protected void getStateMainDashboard(Form f, Hashtable h) {
    }


    protected void getStateSurveyDetails(Form f, Hashtable h) {
    }


    protected void getStateNonQualitativeReport(Form f, Hashtable h) {
    }


    protected void getStateSignInForm(Form f, Hashtable h) {
    }


    protected void getStateSurveys(Form f, Hashtable h) {
    }


    protected void getStateMain(Form f, Hashtable h) {
    }

    protected void setFormState(Form f, Hashtable state) {
        super.setFormState(f, state);
        if("Login".equals(f.getName())) {
            setStateLogin(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("Monitoring".equals(f.getName())) {
            setStateMonitoring(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("Dashboard".equals(f.getName())) {
            setStateDashboard(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("MainMenu".equals(f.getName())) {
            setStateMainMenu(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("QualitativeReport".equals(f.getName())) {
            setStateQualitativeReport(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("Test".equals(f.getName())) {
            setStateTest(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("MainDashboard".equals(f.getName())) {
            setStateMainDashboard(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("SurveyDetails".equals(f.getName())) {
            setStateSurveyDetails(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("NonQualitativeReport".equals(f.getName())) {
            setStateNonQualitativeReport(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("SignInForm".equals(f.getName())) {
            setStateSignInForm(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("Surveys".equals(f.getName())) {
            setStateSurveys(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("Main".equals(f.getName())) {
            setStateMain(f, state);
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void setStateLogin(Form f, Hashtable state) {
    }


    protected void setStateMonitoring(Form f, Hashtable state) {
    }


    protected void setStateDashboard(Form f, Hashtable state) {
    }


    protected void setStateMainMenu(Form f, Hashtable state) {
    }


    protected void setStateQualitativeReport(Form f, Hashtable state) {
    }


    protected void setStateTest(Form f, Hashtable state) {
    }


    protected void setStateMainDashboard(Form f, Hashtable state) {
    }


    protected void setStateSurveyDetails(Form f, Hashtable state) {
    }


    protected void setStateNonQualitativeReport(Form f, Hashtable state) {
    }


    protected void setStateSignInForm(Form f, Hashtable state) {
    }


    protected void setStateSurveys(Form f, Hashtable state) {
    }


    protected void setStateMain(Form f, Hashtable state) {
    }

    protected void handleComponentAction(Component c, ActionEvent event) {
        Container rootContainerAncestor = getRootAncestor(c);
        if(rootContainerAncestor == null) return;
        String rootContainerName = rootContainerAncestor.getName();
        Container leadParentContainer = c.getParent().getLeadParent();
        if(leadParentContainer != null && leadParentContainer.getClass() != Container.class) {
            c = c.getParent().getLeadParent();
        }
        if(rootContainerName == null) return;
        if(rootContainerName.equals("Login")) {
            if("TextField".equals(c.getName())) {
                onLogin_TextFieldAction(c, event);
                return;
            }
            if("TextField1".equals(c.getName())) {
                onLogin_TextField1Action(c, event);
                return;
            }
            if("Button".equals(c.getName())) {
                onLogin_ButtonAction(c, event);
                return;
            }
        }
        if(rootContainerName.equals("Monitoring")) {
            if("Button".equals(c.getName())) {
                onMonitoring_ButtonAction(c, event);
                return;
            }
            if("Button1".equals(c.getName())) {
                onMonitoring_Button1Action(c, event);
                return;
            }
        }
        if(rootContainerName.equals("MainMenu")) {
            if("Button".equals(c.getName())) {
                onMainMenu_ButtonAction(c, event);
                return;
            }
            if("Button1".equals(c.getName())) {
                onMainMenu_Button1Action(c, event);
                return;
            }
            if("Button2".equals(c.getName())) {
                onMainMenu_Button2Action(c, event);
                return;
            }
        }
        if(rootContainerName.equals("Surveys")) {
            if("Button".equals(c.getName())) {
                onSurveys_ButtonAction(c, event);
                return;
            }
            if("Button1".equals(c.getName())) {
                onSurveys_Button1Action(c, event);
                return;
            }
        }
    }

      protected void onLogin_TextFieldAction(Component c, ActionEvent event) {
      }

      protected void onLogin_TextField1Action(Component c, ActionEvent event) {
      }

      protected void onLogin_ButtonAction(Component c, ActionEvent event) {
      }

      protected void onMonitoring_ButtonAction(Component c, ActionEvent event) {
      }

      protected void onMonitoring_Button1Action(Component c, ActionEvent event) {
      }

      protected void onMainMenu_ButtonAction(Component c, ActionEvent event) {
      }

      protected void onMainMenu_Button1Action(Component c, ActionEvent event) {
      }

      protected void onMainMenu_Button2Action(Component c, ActionEvent event) {
      }

      protected void onSurveys_ButtonAction(Component c, ActionEvent event) {
      }

      protected void onSurveys_Button1Action(Component c, ActionEvent event) {
      }

}
