/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package al.cfm.views;

import java.util.ArrayList;

/**
 *
 * @author dbishani
 */
public class PieChartElement {

    int index;
    String description;
    int percent;
    ArrayList<ProgressBarDetails> progressBarPercent = new ArrayList<>();

    public int getIndex() {
        return index;
    }

    public String getDescription() {
        return description;
    }

    public int getPercent() {
        return percent;
    }

    public ArrayList<ProgressBarDetails> getProgressBarPercent() {
        return progressBarPercent;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public void setProgressBarPercent(ArrayList<ProgressBarDetails> progressBarPercent) {
        this.progressBarPercent = progressBarPercent;
    }

}
