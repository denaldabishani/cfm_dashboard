/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package al.cfm.views;

import al.cfm.views.PieChartElement;
import java.util.ArrayList;

/**
 *
 * @author dbishani
 */
public class PieChartElementsMockUp {

    private static ArrayList<PieChartElement> pieChartElementsList = new ArrayList<>();

    public static ArrayList<PieChartElement> getPieChartElementsList() {

        PieChartElement element1 = new PieChartElement();
        ArrayList<ProgressBarDetails> progressBarDetailsList1 = new ArrayList<>();
        progressBarDetailsList1.add(new ProgressBarDetails("Appreciation", 56));
        progressBarDetailsList1.add(new ProgressBarDetails("Improvement", 1));
        progressBarDetailsList1.add(new ProgressBarDetails("Thankfull", 3));
        progressBarDetailsList1.add(new ProgressBarDetails("Back Office", 27));
        progressBarDetailsList1.add(new ProgressBarDetails("Front Office", 4));
        progressBarDetailsList1.add(new ProgressBarDetails("Multi Returns", 1));
        progressBarDetailsList1.add(new ProgressBarDetails("Other", 5));
        progressBarDetailsList1.add(new ProgressBarDetails("Corruption", 1));
        element1.setIndex(0);
        element1.setDescription("Qualitative");
        element1.setPercent(61);
        element1.setProgressBarPercent(progressBarDetailsList1);
        pieChartElementsList.add(element1);

        PieChartElement element2 = new PieChartElement();
        ArrayList<ProgressBarDetails> progressBarDetailsList2 = new ArrayList<>();
        progressBarDetailsList2.add(new ProgressBarDetails("Back Office", 27));
        progressBarDetailsList2.add(new ProgressBarDetails("Front Office", 4));
        progressBarDetailsList2.add(new ProgressBarDetails("Multi Returns", 1));
        progressBarDetailsList2.add(new ProgressBarDetails("Other", 5));
        progressBarDetailsList2.add(new ProgressBarDetails("Corruption", 1));
        element2.setIndex(0);
        element2.setDescription("Non Qualitative");
        element2.setPercent(31);
        element2.setProgressBarPercent(progressBarDetailsList2);
        pieChartElementsList.add(element2);

        return pieChartElementsList;

    }
}
