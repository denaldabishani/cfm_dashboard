/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package al.cfm.views;

/**
 *
 * @author dbishani
 */
public class ProgressBarDetails {

    private String description;
    private int percent;

    public ProgressBarDetails(String description, int percent) {
        this.description = description;
        this.percent = percent;
    }

    public String getDescription() {
        return description;
    }

    public int getPercent() {
        return percent;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

}
