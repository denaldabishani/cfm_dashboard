/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package al.cfm.views;

import com.codename1.charts.util.ColorUtil;
import com.codename1.ui.Component;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.Graphics;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.plaf.Style;

/**
 *
 * @author egorari
 */
public class Progress extends Component {

    private byte percent;

    public Progress() {
        setFocusable(false);
    }

    public String getUIID() {
        return "Progress";
    }

    public byte getProgress() {
        return percent;
    }

    public void setProgress(byte percent) {
        this.percent = percent;
        repaint();
    }

    protected Dimension calcPreferredSize() {
        // we don't really need to be in the font height but this provides
        // a generally good indication for size expectations
        return new Dimension(Display.getInstance().getDisplayWidth(),
                Font.getDefaultFont().getHeight());

    }

    public void paint(Graphics g) {
        int width = (int) ((((float) percent) / 100.0f) * getWidth());

        // draw based on simple graphics primitives
        Style s = getStyle();
        g.setColor(s.getBgColor());
        int curve = getHeight() / 2 - 1;
        g.fillRoundRect(getX(), getY(), getWidth() - 1, getHeight() - 1, curve, curve);
        g.setColor(s.getFgColor());
        //g.drawRoundRect(getX(), getY(), getWidth() - 1, getHeight() - 1, curve, curve);
        g.clipRect(getX(), getY(), width - 1, getHeight() - 1);
        g.setColor(ColorUtil.GREEN);
        g.fillRoundRect(getX(), getY(), getWidth() - 1, getHeight() - 1, curve, curve);

    }
}
